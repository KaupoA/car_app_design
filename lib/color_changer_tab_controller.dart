import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';

class ColorChangerTabController extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ColorChangerTabState();
  }
}

class _ColorChangerTabState extends State<ColorChangerTabController> {
  var assets = [
    'assets/images/bmw.png',
    'assets/images/bmw-blue.jpg',
    'assets/images/bmw-red.jpg'
  ];
  var _index = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(top: 50),
            child: Image.asset(assets[_index]),
          ),
          DefaultTabController(
            length: 3,
            initialIndex: _index,
            child: Padding(
              padding: EdgeInsets.only(top: 20),
              child: Container(
                height: 100,
                width: 200,
                child: TabBar(
                  indicator: new BubbleTabIndicator(
                    indicatorHeight: 55,
                    indicatorColor: Colors.grey[200],
                    tabBarIndicatorSize: TabBarIndicatorSize.tab,
                  ),
                  onTap: (int n) {
                    setState(() {
                      _index = n;
                    });
                  },
                  tabs: <Widget>[
                    SizedBox(
                      child: FloatingActionButton(
                        backgroundColor: Colors.white,
                      ),
                    ),
                    SizedBox(
                      child: FloatingActionButton(
                        backgroundColor: Colors.black,
                      ),
                    ),
                    SizedBox(
                      child: FloatingActionButton(
                        backgroundColor: Colors.red[900],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
