import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:carappdesign/color_changer_tab_controller.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Car App Design',
      theme: ThemeData(primarySwatch: Colors.blue, buttonColor: Colors.blue),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
        title: Text('BMW 5 Series'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 40),
                  child: Text('BMW 5 Series',
                      style:
                      TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
                ),
                DefaultTabController(
                  length: 3,
                  child: Padding(
                    padding: EdgeInsets.only(left: 32, right: 32, top: 20),
                    child: Container(
                      height: 450,
                      width: double.infinity,
                      child: Column(
                        children: <Widget>[
                          TabBar(
                            labelColor: Theme
                                .of(context)
                                .accentColor,
                            unselectedLabelColor: Colors.grey[300],
                            tabs: <Widget>[
                              Tab(
                                text: "Exterior",
                              ),
                              Tab(
                                text: "Interior",
                              ),
                              Tab(
                                text: "Engines",
                              ),
                            ],
                          ),
                          Expanded(
                            child: TabBarView(
                              children: <Widget>[
                                ColorChangerTabController(),
                                Container(
                                  color: Colors.grey,
                                ),
                                Container(
                                    color: Colors.red
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border(
                        top: BorderSide(width: 1, color: Colors.grey[200])),
                  ),
                ),
              ],
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 30),
                child: Text(
                  'Price from',
                  style: TextStyle(fontSize: 10, color: Colors.grey[350]),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 15, left: 30),
                child: Text(
                  '4299€',
                  style: TextStyle(fontSize: 24),
                ),
              )
            ],
          ),
        ],
      ),
      floatingActionButton: Container(
        height: 40,
        width: 120,
        child: RaisedButton(
          child: Text('Next'),
          onPressed: () {},
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          elevation: 8,
          textColor: Colors.white,
        ),
      ),
    );
  }
}
